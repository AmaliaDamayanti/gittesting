package com.ad.bekup_pertemuan1;

import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //membuat tulisan bergaris bawah
        textView= (TextView) findViewById(R.id.textView4);
        textView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
    }
}
